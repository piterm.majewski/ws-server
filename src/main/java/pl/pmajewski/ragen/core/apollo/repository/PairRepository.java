package pl.pmajewski.ragen.core.apollo.repository;

import org.springframework.data.repository.CrudRepository;
import pl.pmajewski.ragen.core.apollo.model.Pair;

public interface PairRepository extends CrudRepository<Pair, Long>, PairRepositoryCustom {

}
