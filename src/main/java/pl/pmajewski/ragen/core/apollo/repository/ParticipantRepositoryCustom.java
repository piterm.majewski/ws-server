package pl.pmajewski.ragen.core.apollo.repository;

import pl.pmajewski.ragen.core.apollo.model.Participant;

import java.util.Optional;

public interface ParticipantRepositoryCustom {

    Optional<Participant> getFirst();

    Optional<Participant> getFirstOtherThan(Participant participant);
}
