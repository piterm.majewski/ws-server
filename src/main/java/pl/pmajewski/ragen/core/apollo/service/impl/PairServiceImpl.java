package pl.pmajewski.ragen.core.apollo.service.impl;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.pmajewski.ragen.core.apollo.model.Pair;
import pl.pmajewski.ragen.core.apollo.model.Participant;
import pl.pmajewski.ragen.core.apollo.model.PreferencesHistory;
import pl.pmajewski.ragen.core.apollo.pojo.ApolloMessagePojo;
import pl.pmajewski.ragen.core.apollo.repository.PairRepository;
import pl.pmajewski.ragen.core.apollo.repository.ParticipantRepository;
import pl.pmajewski.ragen.core.apollo.service.PairService;
import pl.pmajewski.ragen.core.config.websocket.pojo.ChatResponse;
import pl.pmajewski.ragen.core.initializer.model.DeviceIdentification;
import pl.pmajewski.ragen.core.initializer.repository.DeviceIdentificationRepository;
import pl.pmajewski.ragen.core.utils.MessageResponseCode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Optional;

@Service
@Transactional
@Setter
public class PairServiceImpl implements PairService {

    @Autowired
    private PairRepository pairRepository;
    @Autowired
    private SimpMessagingTemplate simpTemplate;
    @Autowired
    private DeviceIdentificationRepository deviceIdentificationRepository;
    @Autowired
    private ParticipantRepository participantRepository;

    @Override
    public Long getActivePairDevice(Long deviceIdentification) {
        Optional<DeviceIdentification> device = deviceIdentificationRepository.findById(deviceIdentification);
        Optional<Pair> pair = pairRepository.getLastPairByDeviceIdentification(device.get());
        return getPairDeviceIdentification(pair.get(), device.get());
    }

    private Long getPairDeviceIdentification(Pair pair, DeviceIdentification identification) {
        if(pair.getDeviceOne().equals(identification)) {
            return pair.getDeviceTwo().getId();
        } else {
            return pair.getDeviceOne().getId();
        }
    }

    @Override
    public void announceNewPair(Long participantOneId, Long participantTwoId) {
        Optional<Participant> participantOne = participantRepository.findById(participantOneId);
        Optional<Participant> participantTwo = participantRepository.findById(participantTwoId);


        Pair pair = new Pair();
        pair.setStartTimestamp(Calendar.getInstance());
        pair.setDeviceOne(participantOne.get().getIdentification());
        pair.setDeviceTwo(participantTwo.get().getIdentification());

        PreferencesHistory historyOne = getPreferencesHistory(participantOne.get(), pair);
        PreferencesHistory historyTwo = getPreferencesHistory(participantTwo.get(), pair);

        pair.setPreferencesHistories(new ArrayList<>(Arrays.asList(historyOne, historyTwo)));

        pairRepository.save(pair);
        participantRepository.deleteAll(Arrays.asList(participantOne.get(), participantTwo.get()));

        simpTemplate.convertAndSend("/queue/apollo/"+pair.getDeviceOne().getId()+"/participant/start", new ChatResponse(MessageResponseCode.APOLLO_PARTICIPANT_FOUNDED, ""));
        simpTemplate.convertAndSend("/queue/apollo/"+pair.getDeviceTwo().getId()+"/participant/start", new ChatResponse(MessageResponseCode.APOLLO_PARTICIPANT_FOUNDED, ""));
    }

    private PreferencesHistory getPreferencesHistory(Participant participant, Pair pair) {
        PreferencesHistory p = new PreferencesHistory();

        p.setDeviceIdentification(participant.getIdentification());
        p.setPair(pair);
        p.setSex(participant.getSex());
        p.setLongitude(participant.getLongitude());
        p.setLatitude(participant.getLatitude());
        p.setPreferedSex(participant.getPreferedSex());
        p.setPair(pair);

        return p;
    }

    @Override
    public void disconnect(Long deviceIdentification) {
        Optional<DeviceIdentification> device = deviceIdentificationRepository.findById(deviceIdentification);
        Optional<Pair> pair = pairRepository.getLastPairByDeviceIdentification(device.get());
        pair.ifPresent(i -> {
            disconnectByPairId(i.getId());
            i.setEndTimestamp(Calendar.getInstance());
        });
    }

    @Override
    public void disconnectByPairId(Long pairId) {
        Optional<Pair> pair =  pairRepository.findById(pairId);
        pair.ifPresent(i -> {
            i.setEndTimestamp(Calendar.getInstance());

            simpTemplate.convertAndSend("/queue/apollo/"+i.getDeviceOne().getId()+"/error", new ChatResponse(MessageResponseCode.APOLLO_PARTICIPANT_DISCONNECTED, "Participant disconnected"));
            simpTemplate.convertAndSend("/queue/apollo/"+i.getDeviceTwo().getId()+"/error", new ChatResponse(MessageResponseCode.APOLLO_PARTICIPANT_DISCONNECTED, "Participant disconnected"));
        });
    }

    @Override
    public void sendMessage(Long deviceIdentificationId, ApolloMessagePojo message) {
        simpTemplate.convertAndSend("/queue/apollo/"+deviceIdentificationId+"/messages", new ChatResponse<ApolloMessagePojo>(MessageResponseCode.APOLLO_CHAT_MESSAGE, message));
    }
}
