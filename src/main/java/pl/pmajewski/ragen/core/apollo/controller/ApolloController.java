package pl.pmajewski.ragen.core.apollo.controller;

import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.stereotype.Controller;
import pl.pmajewski.ragen.core.apollo.pojo.ApolloMessagePojo;
import pl.pmajewski.ragen.core.apollo.pojo.RandomChatInitPojo;
import pl.pmajewski.ragen.core.apollo.service.PairService;
import pl.pmajewski.ragen.core.apollo.service.ParticipantService;
import pl.pmajewski.ragen.core.config.websocket.model.WebsocketSessionData;

@Controller
@CommonsLog
public class ApolloController {

    @Autowired
    private WebsocketSessionData wsData;
    @Autowired
    private ParticipantService participantService;
    @Autowired
    private PairService pairService;

    @SubscribeMapping("/queue/apollo/{id}/messages")
    public void subscribeRandomChat(SimpMessageHeaderAccessor headerAccessor) {
        log.info("[device-"+wsData.getInitializerId()+"] subscribe apollo");
    }

    @MessageMapping("/apollo/participant/search")
    public void randParticipant(@Payload RandomChatInitPojo payload) {
        log.info("Search participant request from "+wsData.getInitializerId());
        participantService.addToActive(wsData.getInitializerId(), payload);
    }

    @MessageMapping("/apollo/participant/cancel")
    public void cancelParticipant() {
        log.info("Device "+wsData.getInitializerId()+" cancel participant");
        participantService.removeFromActive(wsData.getInitializerId());
        pairService.disconnect(wsData.getInitializerId());
    }

    @MessageMapping("/apollo/participant/messages/send")
    public void sendMessage(@Payload ApolloMessagePojo message) {
        log.info("Handle message from deviceIdentification: "+wsData.getInitializerId());
        Long participant = pairService.getActivePairDevice(wsData.getInitializerId());
        pairService.sendMessage(participant, message);
    }
}