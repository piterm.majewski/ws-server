package pl.pmajewski.ragen.core.apollo.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;
import pl.pmajewski.ragen.core.utils.Sex;

@Data
@NoArgsConstructor
public class RandomChatInitPojo {

    private Sex sex;
    private String longitude;
    private String latitude;
    private Sex preferedSex;
}
