package pl.pmajewski.ragen.core.apollo.randomizer;

import lombok.AllArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.pmajewski.ragen.core.apollo.model.Participant;
import pl.pmajewski.ragen.core.apollo.repository.ParticipantRepository;
import pl.pmajewski.ragen.core.apollo.service.PairService;

import java.util.Optional;

@Component
@AllArgsConstructor
@Transactional
@CommonsLog
public class Randomizer {

    private ParticipantRepository participantRepository;
    private PairService pairService;

    @Scheduled(fixedDelay = 25)
    private void pairParticipants() {
        Optional<Participant> first = participantRepository.getFirst();
        Optional<Participant> sec = Optional.empty();
        if(first.isPresent()) {
            sec = participantRepository.getFirstOtherThan(first.get());
        }

        if(first.isPresent() && sec.isPresent()) {
            pairService.announceNewPair(first.get().getId(), sec.get().getId());
        }
    }
}
