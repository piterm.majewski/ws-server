package pl.pmajewski.ragen.core.apollo.service;

import pl.pmajewski.ragen.core.apollo.pojo.RandomChatInitPojo;

public interface ParticipantService {

    void addToActive(Long initializerId, RandomChatInitPojo preferences);

    void removeFromActive(Long initializerId);
}
