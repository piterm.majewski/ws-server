package pl.pmajewski.ragen.core.apollo.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import pl.pmajewski.ragen.core.apollo.model.Participant;
import pl.pmajewski.ragen.core.apollo.pojo.RandomChatInitPojo;
import pl.pmajewski.ragen.core.apollo.repository.ParticipantRepository;
import pl.pmajewski.ragen.core.apollo.service.ParticipantService;
import pl.pmajewski.ragen.core.initializer.model.DeviceIdentification;
import pl.pmajewski.ragen.core.initializer.repository.DeviceIdentificationRepository;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
@AllArgsConstructor
public class ParticipantServiceImpl implements ParticipantService {

    private ParticipantRepository participantRepository;
    private DeviceIdentificationRepository deviceIdentificationRepository;

    @Override
    public void addToActive(Long initializerId, RandomChatInitPojo preferences) {
        Optional<DeviceIdentification> device = deviceIdentificationRepository.findById(initializerId);

        Participant participant = new Participant();
        participant.setIdentification(device.get());
        participant.setPreferedSex(preferences.getPreferedSex());
        participant.setSex(preferences.getSex());

        participantRepository.save(participant);
    }

    @Override
    public void removeFromActive(Long initializerId) {
        Optional<DeviceIdentification> deviceIdentification = deviceIdentificationRepository.findById(initializerId);
        Optional<Participant> participant = participantRepository.getByIdentification(deviceIdentification.get());
        participantRepository.delete(participant.get());

    }
}
