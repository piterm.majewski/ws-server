package pl.pmajewski.ragen.core.apollo.repository;

import pl.pmajewski.ragen.core.apollo.model.Pair;
import pl.pmajewski.ragen.core.initializer.model.DeviceIdentification;

import java.util.Optional;

public interface PairRepositoryCustom {

    Optional<Pair> getLastPairByDeviceIdentification(DeviceIdentification deviceIdentification);
}
