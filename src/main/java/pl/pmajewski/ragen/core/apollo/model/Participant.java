package pl.pmajewski.ragen.core.apollo.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import pl.pmajewski.ragen.core.initializer.model.DeviceIdentification;
import pl.pmajewski.ragen.core.utils.Sex;

import javax.persistence.*;
import java.util.Calendar;

@Entity
@Table(name = "participants", schema = "randomizer")
@Data
@EqualsAndHashCode(of = "id")
public class Participant {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "identification_id")
    private DeviceIdentification identification;

    @Column(name = "timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar timestamp;

    @Column
    @Enumerated(EnumType.STRING)
    private Sex sex;

    @Column(name =  "longitude")
    private String longitude;

    @Column(name = "latitude")
    private String latitude;

    @Column
    @Enumerated(EnumType.STRING)
    private Sex preferedSex;

    @PrePersist
    private void prePersist() {
        this.timestamp = Calendar.getInstance();
    }
}