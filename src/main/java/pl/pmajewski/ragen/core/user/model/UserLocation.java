package pl.pmajewski.ragen.core.user.model;

import java.util.Calendar;
import java.util.TimeZone;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "user_location", schema = "public")
@Data
@ToString(exclude = {"user"})
@EqualsAndHashCode(of = {"id"})
@RequiredArgsConstructor
@NoArgsConstructor
public class UserLocation {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long id;
	
	@Column
	@NonNull 
	private Double latitude;
	
	@Column
	@NonNull
	private Double longitude;
	
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar timestamp;
	
	@OneToOne(fetch = FetchType.LAZY)
	@NonNull
	private User user;
	
	@PrePersist
	private void prePersist() {
		timestamp = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
	}
}
