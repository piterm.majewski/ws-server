package pl.pmajewski.ragen.core.user.utils.randomizer.exceptions;

public class ConverserSelectedAlreadyException extends Exception {

	private static final long serialVersionUID = 1L;

	public ConverserSelectedAlreadyException() {
		super();
	}

	public ConverserSelectedAlreadyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ConverserSelectedAlreadyException(String message, Throwable cause) {
		super(message, cause);
	}

	public ConverserSelectedAlreadyException(String message) {
		super(message);
	}

	public ConverserSelectedAlreadyException(Throwable cause) {
		super(cause);
	}
}
