package pl.pmajewski.ragen.core.user.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import pl.pmajewski.ragen.core.user.model.User;
import pl.pmajewski.ragen.core.user.model.User.Sex;
import pl.pmajewski.ragen.core.user.model.User.Status;

public interface UserRepository extends CrudRepository<User, Long> {
	
	List<User> findByStatusAndSexAndInterested(Status status, Sex sex, Sex interested);
	
	List<User> findByStatusAndInterested(Status status, Sex interested);
}
