package pl.pmajewski.ragen.core.user.model;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(schema = "public", name = "logged_users")
@Inheritance(strategy = InheritanceType.JOINED)
@Data
@EqualsAndHashCode(callSuper = true, of = {})
public class LoggedUser extends User {

	@Column(name = "username")
	@Length(max = 128)
	private String username;
	
	@Column(name = "email")
	@Length(max = 256)
	private String email;
	
	@Column(name = "password")
	@Length(max = 1024)
	private String password;
	
	@Column(name = "salt")
	@Length(max = 1024)
	private String salt;
	
	@Column(name = "registration_date")
	private Calendar registrationDate;
	
	@Column(name = "verified")
	private Boolean verified;
	
	@Column(name = "online")
	private Boolean online;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, mappedBy = "user")
	private Set<UserLocationHistory> locationsHistory;

	public void addLocation(UserLocation location) {
		super.addLocation(location);
		if(locationsHistory == null) {
			locationsHistory = new HashSet<>();
			locationsHistory.add(getUserLocationHistory(location));
		} else {
			locationsHistory.add(getUserLocationHistory(location));
		}
	}
	
	private UserLocationHistory getUserLocationHistory(UserLocation userLocation) {
		return new UserLocationHistory(userLocation.getLatitude(), userLocation.getLongitude(), userLocation.getUser());
	}
}
