package pl.pmajewski.ragen.core.user.service.impl;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import pl.pmajewski.ragen.core.user.exceptions.UserNotFoundException;
import pl.pmajewski.ragen.core.user.model.User;
import pl.pmajewski.ragen.core.user.model.User.Sex;
import pl.pmajewski.ragen.core.user.model.User.Status;
import pl.pmajewski.ragen.core.user.model.UserLocation;
import pl.pmajewski.ragen.core.user.pojo.UserSimplePojo;
import pl.pmajewski.ragen.core.user.pojo.rest.LogInPojo;
import pl.pmajewski.ragen.core.user.repository.UserRepository;
import pl.pmajewski.ragen.core.user.service.UserAnonymousService;

@Service
public class UserAnonymousServiceImpl implements UserAnonymousService {
	
	private UserRepository userRepository;
	
	public UserAnonymousServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	@Transactional
	public UserSimplePojo initialize(LogInPojo body) {
		User user = new User();
		user.setInterested(Sex.valueOf(body.getInterested()));
		user.setSex(Sex.valueOf(body.getSex()));
		user.setStatus(Status.SUSPEND);
		
		UserLocation location = new UserLocation(body.getLatitude(), body.getLongitude(), user);
		user.addLocation(location);
		
		userRepository.save(user);
		return user.getSimplePojo();
	}

	@Override
	@Transactional
	public void setUserOnline(Long userId) {
		User user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException("User with id="+userId+" not found."));
		user.setOnline(true);
	}

	@Override
	@Transactional
	public void setUserOffline(Long userId) {
		User user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException("User with id="+userId+" not found."));
		user.setOnline(false);
	}
}
