package pl.pmajewski.ragen.core.user.repository;

import org.springframework.data.repository.CrudRepository;

import pl.pmajewski.ragen.core.user.model.LoggedUser;

public interface LoggedUserRepository extends CrudRepository<LoggedUser, Long> {

}
