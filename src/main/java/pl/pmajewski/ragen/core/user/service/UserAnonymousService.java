package pl.pmajewski.ragen.core.user.service;

import pl.pmajewski.ragen.core.user.pojo.UserSimplePojo;
import pl.pmajewski.ragen.core.user.pojo.rest.LogInPojo;

public interface UserAnonymousService {
	
	UserSimplePojo initialize(LogInPojo body);
	
	void setUserOnline(Long userId);
	
	void setUserOffline(Long userId);
}
