package pl.pmajewski.ragen.core.utils;

public enum Sex {
    MALE, FEMALE;
}