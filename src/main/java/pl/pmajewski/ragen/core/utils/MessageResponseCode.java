package pl.pmajewski.ragen.core.utils;

/**
 * Codes based on HttpStatus - spring enum
 *
 * Custom App code have numbers greater than 1000
 *
 * <1001, 1100> - Apollo range
 */
public enum MessageResponseCode {

    CONTINUE(100, "Continue"),
    OK(200, "OK"),
    CREATED(201, "Created"),
    APOLLO_PARTICIPANT_FOUNDED(1001, "Paritcipant founded"),
    APOLLO_PARTICIPANT_DISCONNECTED(1002, "Participant disconnected"),
    APOLLO_CHAT_MESSAGE(1003, "Apollo chat message");


    private Integer code;
    private String description;

    private MessageResponseCode(Integer code, String description) {
        this.code = code;
        this.description = description;
    }

    public Integer getCode() {
        return this.code;
    }

    @Override
    public String toString() {
        return  this.code.toString();
    }
}
