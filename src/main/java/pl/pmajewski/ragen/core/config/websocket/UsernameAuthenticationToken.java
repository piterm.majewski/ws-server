package pl.pmajewski.ragen.core.config.websocket;

import lombok.Data;

import javax.security.auth.Subject;
import java.security.Principal;

@Data
public class UsernameAuthenticationToken implements Principal {

    private String username;

    public UsernameAuthenticationToken(String username) {
        this.username = username;
    }

    @Override
    public String getName() {
        return null;
    }

}
