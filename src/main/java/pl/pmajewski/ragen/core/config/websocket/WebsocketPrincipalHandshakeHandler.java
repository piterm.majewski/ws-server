package pl.pmajewski.ragen.core.config.websocket;

import org.springframework.http.server.ServerHttpRequest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;

import java.security.Principal;
import java.util.Map;
import java.util.Random;

public class WebsocketPrincipalHandshakeHandler extends DefaultHandshakeHandler {

    @Override
    protected Principal determineUser(ServerHttpRequest request, WebSocketHandler wsHandler, Map<String, Object> attributes) {
        Random random = new Random();
        String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();
        System.out.println("WebsocketPrincipalHandshakeHandler - > determineUser() -> sessionId: "+sessionId);
        return new UsernamePasswordAuthenticationToken("lorus", null);
    }
}
