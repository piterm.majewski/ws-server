package pl.pmajewski.ragen.core.config.websocket.pojo;

import lombok.Getter;
import lombok.Setter;
import pl.pmajewski.ragen.core.utils.MessageResponseCode;

@Getter
@Setter
public class ChatResponse <T> {

    private Integer code;
    private String message;
    private T content;

    public ChatResponse(MessageResponseCode code) {
        this.code = code.getCode();
    }

    public ChatResponse(MessageResponseCode code, String message) {
        this.code = code.getCode();
        this.message = message;
    }

    public ChatResponse(MessageResponseCode code, T content) {
        this.code = code.getCode();
        this.content = content;
    }

    public ChatResponse(MessageResponseCode code, String message, T content) {
        this.code = code.getCode();
        this.message = message;
        this.content = content;
    }
}
