package pl.pmajewski.ragen.core.deadsquirrel.pojo;

import lombok.*;
import pl.pmajewski.ragen.core.config.websocket.pojo.ChatResponse;

import java.util.Calendar;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChatMessage {

    private String messageContent;
    private Calendar time;
}
