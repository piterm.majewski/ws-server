package pl.pmajewski.ragen.core.deadsquirrel;

import lombok.Setter;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pl.pmajewski.ragen.core.config.websocket.pojo.ChatResponse;
import pl.pmajewski.ragen.core.deadsquirrel.pojo.ChatMessage;
import pl.pmajewski.ragen.core.utils.MessageResponseCode;

import java.util.Calendar;
import java.util.UUID;

@Component
@CommonsLog
public class ChatScheduler {

	@Autowired
	@Setter
	private SimpMessagingTemplate template;
	
	@Scheduled(fixedDelay = 5000)
	public void randomMessage() {
		ChatResponse<ChatMessage> response = new ChatResponse<>(MessageResponseCode.OK, "", new ChatMessage(UUID.randomUUID().toString(), Calendar.getInstance()));
		template.convertAndSend("/topic/master", response);
		log.info("randomMessage -> response: "+response);
	}
}
