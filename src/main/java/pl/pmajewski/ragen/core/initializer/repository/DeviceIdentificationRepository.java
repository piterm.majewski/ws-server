package pl.pmajewski.ragen.core.initializer.repository;

import org.springframework.data.repository.CrudRepository;
import pl.pmajewski.ragen.core.initializer.model.DeviceIdentification;

public interface DeviceIdentificationRepository extends CrudRepository<DeviceIdentification, Long> {
}
