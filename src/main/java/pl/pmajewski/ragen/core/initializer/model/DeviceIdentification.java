package pl.pmajewski.ragen.core.initializer.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.JoinFormula;
import pl.pmajewski.ragen.core.apollo.model.Pair;
import pl.pmajewski.ragen.core.user.model.User;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "device_identification", schema = "public")
@Data
@EqualsAndHashCode(of = "id")
public class DeviceIdentification {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "device_UUID")
    private String deviceUUID;

    @Column(name = "token")
    private String token;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "connect_timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar connectTimestamp;

    @Column(name = "disconnect_timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar disconnectTimestamp;

    @PrePersist
    private void prePersist() {
        connectTimestamp = Calendar.getInstance();
    }
}
