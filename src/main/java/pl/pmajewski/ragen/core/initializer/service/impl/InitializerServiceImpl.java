package pl.pmajewski.ragen.core.initializer.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.pmajewski.ragen.core.apollo.service.PairService;
import pl.pmajewski.ragen.core.initializer.model.DeviceIdentification;
import pl.pmajewski.ragen.core.initializer.pojo.InitAuthorizationResponsePojo;
import pl.pmajewski.ragen.core.initializer.repository.DeviceIdentificationRepository;
import pl.pmajewski.ragen.core.initializer.service.InitializerService;

import javax.transaction.Transactional;
import java.util.Calendar;
import java.util.Optional;

@Service
@Transactional
@AllArgsConstructor
public class InitializerServiceImpl implements InitializerService {

    private DeviceIdentificationRepository deviceIdentificationRepository;
    private PairService pairService;


    @Override
    public InitAuthorizationResponsePojo initialize(String deviceUUID) {

        DeviceIdentification deviceIdentification = new DeviceIdentification();
        deviceIdentification.setDeviceUUID(deviceUUID);
        deviceIdentificationRepository.save(deviceIdentification);

        return new InitAuthorizationResponsePojo(deviceIdentification.getId());
    }

    @Override
    public void closeConnection(Long initializerId) {
        pairService.disconnect(initializerId);
        Optional<DeviceIdentification> device = deviceIdentificationRepository.findById(initializerId);
        device.get().setDisconnectTimestamp(Calendar.getInstance());
    }
}

