package pl.pmajewski.ragen.core.initializer.controller;

import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.request.RequestContextHolder;
import pl.pmajewski.ragen.core.config.websocket.model.WebsocketSessionData;
import pl.pmajewski.ragen.core.initializer.pojo.InitAuthorizationPojo;
import pl.pmajewski.ragen.core.initializer.pojo.InitAuthorizationResponsePojo;
import pl.pmajewski.ragen.core.initializer.service.InitializerService;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Map;

@Controller
@CommonsLog
public class InitializerController {

    @Autowired
    private InitializerService initializerService;
    @Autowired
    private WebsocketSessionData wsData;

    @MessageMapping("/initializer")
    @SendToUser("/queue/initializer/authentication")
    public InitAuthorizationResponsePojo initizalizeDevice(@Payload InitAuthorizationPojo payload, SimpMessageHeaderAccessor headerAccessor) {
        InitAuthorizationResponsePojo initializePojo = initializerService.initialize(payload.getDeviceId());
        wsData.setInitializerId(initializePojo.getId());
        return initializePojo;
    }
}
