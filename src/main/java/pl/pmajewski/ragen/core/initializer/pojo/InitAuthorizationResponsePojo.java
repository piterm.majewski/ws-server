package pl.pmajewski.ragen.core.initializer.pojo;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InitAuthorizationResponsePojo {

    private Long id;
}
